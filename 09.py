import matplotlib.pyplot as plt
import numpy as np
import random
from matplotlib.animation import FuncAnimation
import mpl_toolkits.mplot3d.axes3d as p3
import copy


class ControlParameters:
    def __init__(self, pop_size, generations, gamma, alpha, function, up_bound, low_bound):
        self.population_size = pop_size
        self.generations = generations
        self.alpha = alpha
        self.gamma = gamma
        self.function = function
        self.up_bound = up_bound
        self.low_bound = low_bound


class Individual:
    def __init__(self, parameters, function):
        self.__parameters = parameters
        self.__best_parameters = copy.deepcopy(parameters)
        self.__parameter_history = [copy.deepcopy(self.__parameters)]
        self.__evaluation = np.inf
        self.__best_evaluation = np.inf
        self.__evaluate(function)
        self.__evaluation_history = [self.__evaluation]
        self.__velocity = np.array([0.0] * len(self.__parameters))

    def __evaluate(self, function):
        self.__evaluation = function(self.__parameters)

    def distance(self, other):
        other_parameters = other.get_parameters()
        dist = 0
        for idx, val in enumerate(self.__parameters):
            dist += (val - other_parameters[idx])**2
        return np.sqrt(dist)

    def light(self, r, gamma):
        i_0 = self.get_evaluation()
        return i_0 * np.e**(-gamma * r)

    def get_coordinates(self, index, is_3D = True):
        x = self.__parameter_history[index][0]
        y = self.__parameter_history[index][1]
        if is_3D:
            z = self.__evaluation_history[index]
            return x, y, z
        else:
            return x, y

    def get_parameters(self):
        return copy.deepcopy(self.__parameters)

    def get_evaluation(self):
        return self.__evaluation

    def update_parameters(self, params, function):
        self.__parameters = params
        self.__evaluate(function)

    def __len__(self):
        return len(self.__parameters)

    @staticmethod
    def create_random(dimension, lower_bound, upper_bound, function):
        parameters = []
        for d in range(dimension):
            parameters.append(random.uniform(lower_bound, upper_bound))
        return Individual(np.array(parameters), function)

    @staticmethod
    def create_from_params(parameters, function):
        return Individual(np.array(parameters), function)


class Population:
    def __init__(self, individuals, control_parameters: ControlParameters):
        self.individuals = individuals
        self.__best_individual = self.get_best_individual()
        self.__control_parameters = control_parameters

    def get_best_evaluation(self):
        best_evaluation = np.inf
        for individual in self.individuals:
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
        return best_evaluation

    def get_best_evaluation_idx(self):
        best_evaluation = np.inf
        best_idx = 0
        for idx, individual in enumerate(self.individuals):
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
                best_idx = idx
        return best_idx

    def get_best_individual(self):
        best_evaluation = np.inf
        best_individual = None
        for individual in self.individuals:
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
                best_individual = individual
        return best_individual

    def get_individuals(self):
        return self.individuals

    def get_individual(self, idx) -> Individual:
        return self.individuals[idx]

    def set_individual(self, idx, individual):
        self.individuals[idx] = individual

    def __len__(self):
        return len(self.individuals)


class Solution:
    def __init__(self, dimension, lower_bound, upper_bound):
        self.dimension = dimension
        self.lower_bound = lower_bound  # we will use the same bounds for all parameters
        self.upper_bound = upper_bound

    def __generate_random_population(self, function, control_parameters):
        population = []
        for i in range(control_parameters.population_size):
            population.append(Individual.create_random(self.dimension, self.lower_bound, self.upper_bound, function))
        return Population(population, control_parameters)

    def __update_by_boundaries(self, params):
        for i, param in enumerate(params):
            if param < self.lower_bound:
                params[i] = self.lower_bound
            elif param > self.upper_bound:
                params[i] = self.upper_bound
        return params

    def firefly_algorithm(self, function, control_parameters: ControlParameters):
        population = self.__generate_random_population(function, control_parameters)

        min_values = []
        population_history = []

        best_idx = population.get_best_evaluation_idx()

        for generation in range(control_parameters.generations):
            for i in range(control_parameters.population_size):
                for j in range(control_parameters.population_size):
                    if i == j:
                        continue
                    firefly_j = population.get_individual(j)

                    if j == best_idx:
                        vector = np.random.normal(0, 1, self.dimension)
                        params = firefly_j.get_parameters() + control_parameters.alpha * vector
                        new_individual = Individual.create_from_params(params, function)
                        if new_individual.get_evaluation() < firefly_j.get_evaluation():
                            population.set_individual(j, new_individual)
                        continue

                    firefly_i = population.get_individual(i)
                    distance = firefly_i.distance(firefly_j)
                    light_i = firefly_i.light(distance, control_parameters.gamma)
                    light_j = firefly_j.light(distance, control_parameters.gamma)
                    if light_i > light_j:
                        beta = 1 / (1 + distance)
                        params_diff = firefly_j.get_parameters() - firefly_i.get_parameters()
                        vector = np.random.normal(0, 1, self.dimension)
                        params = firefly_i.get_parameters() + beta * params_diff + control_parameters.alpha * vector
                        params = self.__update_by_boundaries(params)
                        firefly_i.update_parameters(params, function)

            min_values.append(population.get_best_evaluation())
            best_idx = population.get_best_evaluation_idx()
            population_history.append(copy.deepcopy(population))

        if self.dimension == 2:
            X, Y, Z = self.get_data_for_draw(function, 0.25)
            self.draw(X, Y, Z, population_history, control_parameters)
            self.draw_heat_map(Z, population_history, control_parameters)
        self.draw_graph_with_minimal_values_found(min_values)

    def draw_graph_with_minimal_values_found(self, real_min_values):
        x = np.arange(len(real_min_values))
        plt.plot(x, real_min_values, color='c', linewidth=1.5, label='Function evaluation')
        plt.xlabel("Number of generations")
        plt.ylabel("Function evaluation")
        plt.legend()
        plt.show()

    def get_data_for_draw(self, function, step):
        x = np.arange(self.lower_bound, self.upper_bound + step, step)
        y = np.arange(self.lower_bound, self.upper_bound + step, step)
        X, Y = np.meshgrid(x, y)
        Z = function([X, Y])
        return X, Y, Z

    def draw(self, X, Y, Z, populations, control_parameters):
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('f(x1, x2)')
        ax.plot_surface(X, Y, Z, alpha=0.2)

        xxs = []
        yys = []
        zzs = []

        for population in populations:
            xs = []
            ys = []
            zs = []
            for individual in population.get_individuals():
                x, y = individual.get_parameters()
                xs.append(x)
                ys.append(y)
                z = individual.get_evaluation()
                zs.append(z)
            xxs.append(xs)
            yys.append(ys)
            zzs.append(zs)

        def update(iteration, points):
            points.set_data_3d(xxs[iteration], yys[iteration], zzs[iteration])
        points, = ax.plot(xxs[0], yys[0], zzs[0], 'o')
        anim = FuncAnimation(fig, update, control_parameters.generations, fargs=[points], interval=200)

        plt.show()

    def draw_heat_map(self, Z, populations, control_parameters):
        fig, ax = plt.subplots()
        ax.imshow(Z)

        plt.xlim(self.lower_bound, self.upper_bound)
        plt.ylim(self.lower_bound, self.upper_bound)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')

        plt.imshow(Z, cmap='Spectral_r', extent=[self.lower_bound, self.upper_bound, self.lower_bound, self.upper_bound])

        xxs = []
        yys = []

        for population in populations:
            xs = []
            ys = []
            for individual in population.get_individuals():
                x, y = individual.get_parameters()
                xs.append(x)
                ys.append(y)
            xxs.append(xs)
            yys.append(ys)

        def update(iteration, point):
            point.set_data(xxs[iteration], yys[iteration])
            return point

        point, = ax.plot(xxs[0], yys[0], 'o', color='k')
        anim = FuncAnimation(fig, update, control_parameters.generations, fargs=[point])
        plt.show()


class Function:
    def sphere(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2
        return sum

    def schwefel(self, params):
        sum = 0.0
        for param in params:
            sum += param * np.sin(np.sqrt(np.abs(param)))
        return (418.9829 * len(params)) - sum

    def rosenbrock(self, params):
        sum = 0.0
        for i in range(len(params) - 1):
            sum += 100 * (params[i + 1] - params[i] ** 2) ** 2 + (params[i] - 1) ** 2
        return sum

    def rastrigin(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2 - 10 * np.cos(2 * np.pi * param)
        return 10 * len(params) + sum

    def griewank(self, params):
        sum = 0.0
        mult = 1.0
        for i, x in enumerate(params):
            sum += x ** 2 / 4000
            mult *= np.cos(x / np.sqrt(i + 1))
        return sum - mult + 1

    def levy(self, params):
        def w(x):
            return 1 + (x - 1) / 4
        sum = 0.0
        for i in range(len(params) - 1):
            sum += (w(params[i]) - 1) ** 2 * (1 + 10 * np.sin(np.pi * w(params[i]) + 1) ** 2) + (w(params[len(params) - 1]) - 1) ** 2 * (1 + np.sin(2 * np.pi * w(params[len(params) - 1])) ** 2)
        return np.sin(np.pi * w(params[0])) ** 2 + sum

    def michalewicz(self, params):
        m = 10
        sum = 0.0
        for i, param in enumerate(params):
            sum += np.sin(param) * np.sin(((i + 1) * param ** 2) / np.pi) ** (2 * m)
        return -sum

    def zakharov(self, params):
        sum1 = 0.0
        sum2 = 0.0
        for i, param in enumerate(params):
            sum1 += param ** 2
            sum2 += 0.5 * (i + 1) * param
        return sum1 + sum2 ** 2 + sum2 ** 4

    def ackley(self, params):
        a = 20.0
        b = 0.2
        c = 2 * np.pi
        sum1 = 0.0
        sum2 = 0.0
        for param in params:
            sum1 += param ** 2
            sum2 += np.cos(c * param)
        return -a * np.exp(-b * np.sqrt(sum1 / len(params))) - np.exp(sum2 / len(params)) + a + np.exp(1)


def run(function, low_bound, up_bound, pop_size, M_max, gamma, alpha):
    control_parameters = ControlParameters(pop_size, M_max, gamma, alpha, Function().sphere, up_bound, low_bound)
    s = Solution(2, low_bound, up_bound)
    s.firefly_algorithm(function, control_parameters)


pop_size = 20
M_max = 200
gamma = 2
alpha = 0.3

run(Function().sphere, -5, 5, pop_size, M_max, gamma, alpha)
run(Function().schwefel, -500, 500, pop_size, M_max, gamma, alpha)
run(Function().rosenbrock, -10, 10, pop_size, M_max, gamma, alpha)
run(Function().rastrigin, -5.12, 5.12, pop_size, M_max, gamma, alpha)
run(Function().griewank, -10, 10, pop_size, M_max, gamma, alpha)
run(Function().levy, -10, 10, pop_size, M_max, gamma, alpha)
run(Function().michalewicz, 0, np.pi, pop_size, M_max, gamma, alpha)
run(Function().zakharov, -10, 10, pop_size, M_max, gamma, alpha)
run(Function().ackley, -32.768, 32.768, pop_size, M_max, gamma, alpha)

