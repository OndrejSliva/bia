import matplotlib.pyplot as plt
import numpy as np
import random
from matplotlib.animation import FuncAnimation
import mpl_toolkits.mplot3d.axes3d as p3
import copy


class Individual:
    def __init__(self, parameters, function):
        self.__parameters = parameters
        self.__evaluation = np.inf
        self.__evaluate(function)

    def __evaluate(self, function):
        self.__evaluation = function(self.__parameters)

    def get_parameters(self):
        return self.__parameters

    def get_evaluation(self):
        return self.__evaluation

    def __len__(self):
        return len(self.__parameters)

    @staticmethod
    def create_random(dimension, lower_bound, upper_bound, function):
        parameters = []
        for i in range(dimension):
            parameters.append(random.uniform(lower_bound, upper_bound))
        return Individual(np.array(parameters), function)


class Population:
    def __init__(self, individuals):
        self.individuals = individuals

    def get_best_evaluation(self):
        best_evaluation = np.inf
        for individual in self.individuals:
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
        return best_evaluation

    def get_individuals(self):
        return self.individuals

    def __len__(self):
        return len(self.individuals)


class Solution:
    def __init__(self, dimension, lower_bound, upper_bound):
        self.dimension = dimension
        self.lower_bound = lower_bound  # we will use the same bounds for all parameters
        self.upper_bound = upper_bound

    def __generate_random_population(self, NP, function):
        population = []
        for i in range(NP):
            population.append(Individual.create_random(self.dimension, self.lower_bound, self.upper_bound, function))
        return population

    def __select_random(self, force_index, count):
        idx1 = idx2 = idx3 = None

        while True:
            new_idx = random.randint(0, count - 1)
            if new_idx != force_index:
                idx1 = new_idx
                break

        while True:
            new_idx = random.randint(0, count - 1)
            if new_idx != force_index and new_idx != idx1:
                idx2 = new_idx
                break

        while True:
            new_idx = random.randint(0, count - 1)
            if new_idx != force_index and new_idx != idx1 and new_idx != idx2:
                idx3 = new_idx
                break

        return idx1, idx2, idx3

    def update_vector_by_boundaries(self, vector):
        for i, value in enumerate(vector):
            if value < self.lower_bound:
                vector[i] += self.upper_bound
            elif value > self.upper_bound:
                vector[i] += self.lower_bound
        return vector

    def differential_evolution(self, function, G_max, NP, CR, F):
        min_values = []

        population = self.__generate_random_population(NP, function)
        populations = []
        population_object = Population(population)
        populations.append(population_object)

        for g in range(G_max):
            new_population = copy.deepcopy(population)  # new generation
            for i, individual in enumerate(population):  # x is also denoted as a target vector
                r1, r2, r3 = self.__select_random(i, NP)
                x_r1 = population[r1].get_parameters()
                x_r2 = population[r2].get_parameters()
                x_r3 = population[r3].get_parameters()
                mutation_vector = (x_r1 - x_r2) * F + x_r3  # mutation vector. TAKE CARE FOR BOUNDARIES!
                mutation_vector = self.update_vector_by_boundaries(mutation_vector)
                trial_vector = np.zeros(self.dimension)  # trial vector
                j_rnd = np.random.randint(0, self.dimension)

                for j in range(self.dimension):
                    if np.random.uniform() < CR or j == j_rnd:
                        trial_vector[j] = mutation_vector[j]  # at least 1 parameter should be from a mutation vector v
                    else:
                        trial_vector[j] = individual.get_parameters()[j]

                new_individual = Individual(trial_vector, function)

                if new_individual.get_evaluation() <= individual.get_evaluation():  # We always accept a solution with the same fitness as a target vector
                    new_population[i] = new_individual
            population = new_population
            population_object = Population(population)
            populations.append(population_object)
            min_values.append(population_object.get_best_evaluation())

        if self.dimension == 2:
            X, Y, Z = self.get_data_for_draw(function, 0.25)
            self.draw(X, Y, Z, populations)
            self.draw_heat_map(Z, populations)
        self.draw_graph_with_minimal_values_found(min_values)

    def draw_graph_with_minimal_values_found(self, real_min_values):
        x = np.arange(len(real_min_values))
        plt.plot(x, real_min_values, color='c', linewidth=1.5, label='Function evaluation')
        plt.xlabel("Number of generations")
        plt.ylabel("Function evaluation")
        plt.legend()
        plt.show()

    def get_data_for_draw(self, function, step):
        x = np.arange(self.lower_bound, self.upper_bound + step, step)
        y = np.arange(self.lower_bound, self.upper_bound + step, step)
        X, Y = np.meshgrid(x, y)
        Z = function([X, Y])
        return X, Y, Z

    def draw(self, X, Y, Z, populations):
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('f(x1, x2)')
        ax.plot_surface(X, Y, Z, alpha=0.2)

        xxs = []
        yys = []
        zzs = []

        for population in populations:
            xs = []
            ys = []
            zs = []
            for individual in population.get_individuals():
                parameters = individual.get_parameters()
                xs.append(parameters[0])
                ys.append(parameters[1])
                zs.append(individual.get_evaluation())
            xxs.append(xs)
            yys.append(ys)
            zzs.append(zs)

        def update(iteration, points):
            points.set_data_3d(xxs[iteration], yys[iteration], zzs[iteration])
        points, = ax.plot(xxs[0], yys[0], zzs[0], 'o')
        anim = FuncAnimation(fig, update, len(populations), fargs=[points], interval=200)

        plt.show()

    def draw_heat_map(self, Z, populations):
        fig, ax = plt.subplots()
        ax.imshow(Z)

        plt.xlim(self.lower_bound, self.upper_bound)
        plt.ylim(self.lower_bound, self.upper_bound)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')

        plt.imshow(Z, cmap='Spectral_r', extent=[self.lower_bound, self.upper_bound, self.lower_bound, self.upper_bound])

        xxs = []
        yys = []

        for population in populations:
            xs = []
            ys = []
            for individual in population.get_individuals():
                parameters = individual.get_parameters()
                xs.append(parameters[0])
                ys.append(parameters[1])
            xxs.append(xs)
            yys.append(ys)

        def update(iteration, point):
            point.set_data(xxs[iteration], yys[iteration])
            return point

        point, = ax.plot(xxs[0], yys[0], 'o', color='k')
        anim = FuncAnimation(fig, update, len(populations), fargs=[point])
        plt.show()


class Function:
    def sphere(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2
        return sum

    def schwefel(self, params):
        sum = 0.0
        for param in params:
            sum += param * np.sin(np.sqrt(np.abs(param)))
        return (418.9829 * len(params)) - sum

    def rosenbrock(self, params):
        sum = 0.0
        for i in range(len(params) - 1):
            sum += 100 * (params[i + 1] - params[i] ** 2) ** 2 + (params[i] - 1) ** 2
        return sum

    def rastrigin(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2 - 10 * np.cos(2 * np.pi * param)
        return 10 * len(params) + sum

    def griewank(self, params):
        sum = 0.0
        mult = 1.0
        for i, x in enumerate(params):
            sum += x ** 2 / 4000
            mult *= np.cos(x / np.sqrt(i + 1))
        return sum - mult + 1

    def levy(self, params):
        def w(x):
            return 1 + (x - 1) / 4
        sum = 0.0
        for i in range(len(params) - 1):
            sum += (w(params[i]) - 1) ** 2 * (1 + 10 * np.sin(np.pi * w(params[i]) + 1) ** 2) + (w(params[len(params) - 1]) - 1) ** 2 * (1 + np.sin(2 * np.pi * w(params[len(params) - 1])) ** 2)
        return np.sin(np.pi * w(params[0])) ** 2 + sum

    def michalewicz(self, params):
        m = 10
        sum = 0.0
        for i, param in enumerate(params):
            sum += np.sin(param) * np.sin(((i + 1) * param ** 2) / np.pi) ** (2 * m)
        return -sum

    def zakharov(self, params):
        sum1 = 0.0
        sum2 = 0.0
        for i, param in enumerate(params):
            sum1 += param ** 2
            sum2 += 0.5 * (i + 1) * param
        return sum1 + sum2 ** 2 + sum2 ** 4

    def ackley(self, params):
        a = 20.0
        b = 0.2
        c = 2 * np.pi
        sum1 = 0.0
        sum2 = 0.0
        for param in params:
            sum1 += param ** 2
            sum2 += np.cos(c * param)
        return -a * np.exp(-b * np.sqrt(sum1 / len(params))) - np.exp(sum2 / len(params)) + a + np.exp(1)


NP = 20
G = 200
F = 0.5
CR = 0.5


s = Solution(2, -5, 5)
s.differential_evolution(Function().sphere, G, NP, CR, F)
s = Solution(2, -500, 500)
s.differential_evolution(Function().schwefel, G, NP, CR, F)
s = Solution(2, -10, 10)
s.differential_evolution(Function().rosenbrock, G, NP, CR, F)
s = Solution(2, -5.12, 5.12)
s.differential_evolution(Function().rastrigin, G, NP, CR, F)
s = Solution(2, -10, 10)
s.differential_evolution(Function().griewank, G, NP, CR, F)
s = Solution(2, -10, 10)
s.differential_evolution(Function().levy, G, NP, CR, F)
s = Solution(2, 0, np.pi)
s.differential_evolution(Function().michalewicz, G, NP, CR, F)
s = Solution(2, -10, 10)
s.differential_evolution(Function().zakharov, G, NP, CR, F)
s = Solution(2, -32.768, 32.768)
s.differential_evolution(Function().ackley, G, NP, CR, F)
