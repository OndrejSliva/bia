import matplotlib.pyplot as plt
import numpy as np
import random
from matplotlib.animation import FuncAnimation
import mpl_toolkits.mplot3d.axes3d as p3


class Solution:
    def __init__(self, dimension, lower_bound, upper_bound):
        self.dimension = dimension
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.parameters = np.zeros(self.dimension)
        self.function_evaluation = np.inf

    # získá náhodné souřadnice pro definovanou dimenzi
    def get_random_coordinates(self, count):
        coordinates = []
        for i in range(count):
            coo = []
            for j in range(self.dimension):
                x = random.uniform(self.lower_bound, self.upper_bound)
                coo.append(x)
            coordinates.append(coo)
        return np.array(coordinates)

    def blind_search(self, function, num_of_iteration=10, num_of_points=10):
        if num_of_iteration < 1 or num_of_points < 1:
            raise ValueError

        min_coordinates = []
        min_values = []
        for i in range(num_of_iteration):
            coordinates = self.get_random_coordinates(num_of_points)
            actual_min_value = np.inf
            actual_min_coordinates = np.zeros(self.dimension)
            for coordination in coordinates:
                value = function(coordination)
                if value < actual_min_value:
                    actual_min_value = value
                    actual_min_coordinates = coordination
            # if mám nové minimum
            if self.function_evaluation > actual_min_value:
                self.function_evaluation = actual_min_value
                self.parameters = actual_min_coordinates

            # postupmně přidává aktuální minimální souřadnici (proto se někdy může animace "zastavit")
            min_coordinates.append(self.parameters)
            min_values.append(self.function_evaluation)

        # pokud je dimense rovna 2, můžeme provést vizualizaci
        if self.dimension == 2:
            self.draw(function, min_coordinates, min_values)

    # funkce pro vykreslení
    def draw(self, function, coordinates, values, step=0.25):
        x = np.arange(self.lower_bound, self.upper_bound, step)
        y = np.arange(self.lower_bound, self.upper_bound, step)
        X, Y = np.meshgrid(x, y)
        Z = function([X, Y])
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('f(x1, x2)')
        ax.plot_surface(X, Y, Z, alpha=0.2)

        # funkce pro pohyb v animaci
        def update(iteration, coordinates, values, point):
            point.set_data(np.array([coordinates[iteration][0], coordinates[iteration][1]]))
            point.set_3d_properties(values[iteration], 'z')
            return point

        point, = ax.plot(coordinates[0][0], coordinates[0][1], values[0], 'o')
        animation = FuncAnimation(fig, update, len(coordinates), fargs=(coordinates, values, point))
        plt.show()


class Function:
    def sphere(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2
        return sum

    def schwefel(self, params):
        sum = 0.0
        for param in params:
            sum += param * np.sin(np.sqrt(np.abs(param)))
        return (418.9829 * len(params)) - sum

    def rosenbrock(self, params):
        sum = 0.0
        for i in range(len(params) - 1):
            sum += 100 * (params[i + 1] - params[i] ** 2) ** 2 + (params[i] - 1) ** 2
        return sum

    def rastrigin(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2 - 10 * np.cos(2 * np.pi * param)
        return 10 * len(params) + sum

    def griewank(self, params):
        sum = 0.0
        mult = 1.0
        for i, x in enumerate(params):
            sum += x ** 2 / 4000
            mult *= np.cos(x / np.sqrt(i + 1))
        return sum - mult + 1

    def levy(self, params):
        def w(x):
            return 1 + (x - 1) / 4
        sum = 0.0
        for i in range(len(params) - 1):
            sum += (w(params[i]) - 1) ** 2 * (1 + 10 * np.sin(np.pi * w(params[i]) + 1) ** 2) + (w(params[len(params) - 1]) - 1) ** 2 * (1 + np.sin(2 * np.pi * w(params[len(params) - 1])) ** 2)
        return np.sin(np.pi * w(params[0])) ** 2 + sum

    def michalewicz(self, params):
        m = 10
        sum = 0.0
        for i, param in enumerate(params):
            sum += np.sin(param) * np.sin(((i + 1) * param ** 2) / np.pi) ** (2 * m)
        return -sum

    def zakharov(self, params):
        sum1 = 0.0
        sum2 = 0.0
        for i, param in enumerate(params):
            sum1 += param ** 2
            sum2 += 0.5 * (i + 1) * param
        return sum1 + sum2 ** 2 + sum2 ** 4

    def ackley(self, params):
        a = 20.0
        b = 0.2
        c = 2 * np.pi
        sum1 = 0.0
        sum2 = 0.0
        for param in params:
            sum1 += param ** 2
            sum2 += np.cos(c * param)
        return -a * np.exp(-b * np.sqrt(sum1 / len(params))) - np.exp(sum2 / len(params)) + a + np.exp(1)


s = Solution(2, -5, 5)
s.blind_search(Function().sphere)
s = Solution(2, -500, 500)
s.blind_search(Function().schwefel)
s = Solution(2, -10, 10)
s.blind_search(Function().rosenbrock)
s = Solution(2, -5.12, 5.12)
s.blind_search(Function().rastrigin)
s = Solution(2, -10, 10)
s.blind_search(Function().griewank)
s = Solution(2, -10, 10)
s.blind_search(Function().levy)
s = Solution(2, 0, np.pi)
s.blind_search(Function().michalewicz)
s = Solution(2, -10, 10)
s.blind_search(Function().zakharov)
s = Solution(2, -32.768, 32.768)
s.blind_search(Function().ackley)
