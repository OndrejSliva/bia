import random
import copy
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

NP = 20
G = 2000
D = 20


class City:
    def __init__(self, x, y, name):
        self.name = name
        self.x = x
        self.y = y

    def __str__(self):
        return f"X: {self.x}, Y: {self.y}, Name: {self.name}"


class Individual:
    def __init__(self, cities):
        self.__cities = cities
        self.__evaluation = np.inf
        self.__evaluate()

    def __evaluate(self):
        self.__evaluation = 0
        for i in range(len(self.__cities)):
            self.__evaluation += np.sqrt((self.__cities[i - 1].x - self.__cities[i].x)**2 + (self.__cities[i - 1].y - self.__cities[i].y)**2)

    def get_evaluation(self):
        if self.__evaluation is None:
            self.__evaluate()
        return self.__evaluation

    def get_cities(self):
        return self.__cities

    def get_city(self, idx):
        return self.__cities[idx]

    def __len__(self):
        return len(self.__cities)

    def change(self, first_idx, second_idx):
        self.__cities[first_idx], self.__cities[second_idx] = self.__cities[second_idx], self.__cities[first_idx]


class Solution:

    def __generate_cities(self, count):
        cities = []
        for i in range(count):
            x = random.uniform(0, 100)
            y = random.uniform(0, 100)
            name = chr(i + 97)
            cities.append(City(x, y, name))
        return cities

    def __get_random_population(self, cities, NP):
        population = []
        for i in range(NP):
            cities_copy = copy.deepcopy(cities)
            random.shuffle(cities_copy)
            population.append(Individual(cities_copy))
        return population

    def __mutate(self, individual):
        idx = random.randint(0, D - 1)
        while True:
            new_idx = random.randint(0, D - 1)
            if new_idx != idx:
                break
        individual.change(idx, new_idx)
        return individual

    def __crossover(self, A, B):
        end_idx = random.randint(0, len(A))
        child_cities = []
        for i in range(end_idx):
            child_cities.append(A.get_city(i))

        for parent_city in B.get_cities():
            found = False
            for child_city in child_cities:
                if child_city.name == parent_city.name:
                    found = True
                if found:
                    break
            if not found:
                child_cities.append(parent_city)

        return Individual(child_cities)

    def __get_parent_B_idx(self, idx):
        while True:
            new_idx = random.randint(0, D - 1)
            if new_idx != idx:
                return new_idx

    def travel_salesman(self, G, NP, D):

        cities = self.__generate_cities(D)
        population = self.__get_random_population(cities, NP)
        best_individuals = [self.__get_best_individual(population)]
        min_len = self.__get_best_individual(population).get_evaluation()
        for i in range(G):
            new_population = copy.deepcopy(population)

            for j in range(NP):
                parent_A = population[j]
                parent_B = population[self.__get_parent_B_idx(j)]
                offspring_AB = self.__crossover(parent_A, parent_B)
                if np.random.uniform() < 0.5:
                    offspring_AB = self.__mutate(offspring_AB)
                if offspring_AB.get_evaluation() < parent_A.get_evaluation():
                    new_population[j] = offspring_AB
            population = new_population
            if min_len > self.__get_best_individual(population).get_evaluation():
                best_individuals.append(self.__get_best_individual(population))
                min_len = self.__get_best_individual(population).get_evaluation()

        self.__visualize_paths(best_individuals)

    def __visualize_paths(self, individuals, repeat_animation=False):
        fig, ax = plt.subplots()
        cities = individuals[0].get_cities()
        ys = [cities[i].y for i in range(len(cities))]
        xs = [cities[i].x for i in range(len(cities))]
        names = [cities[i].name for i in range(len(cities))]

        plt.scatter(xs, ys)
        for i in range(len(names)):
            plt.annotate(names[i], (xs[i], ys[i]), textcoords="offset points", xytext=(0, 10))

        xxs = []
        yys = []

        for individual in individuals:
            cities = individual.get_cities()
            ys = [cities[i].y for i in range(len(cities))]
            xs = [cities[i].x for i in range(len(cities))]
            xs.append(xs[0])
            ys.append(ys[0])
            xxs.append(xs)
            yys.append(ys)

        lines = plt.plot(xxs[0], yys[0])

        def update(iteration, lines):
            lines.set_xdata(xxs[iteration])
            lines.set_ydata(yys[iteration])
        anim = FuncAnimation(fig, update, len(individuals), fargs=(lines), interval=200, repeat=repeat_animation)
        plt.show()

    def __get_best_individual(self, population):
        min = np.inf
        min_idx = None
        for idx, individual in enumerate(population):
            if individual.get_evaluation() < min:
                min_idx = idx
                min = individual.get_evaluation()
        return population[min_idx]


s = Solution()
s.travel_salesman(G, NP, D)
