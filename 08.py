import random
import copy
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation


class City:
    def __init__(self, x, y, name):
        self.name = name
        self.x = x
        self.y = y

    def __str__(self):
        return f"X: {self.x}, Y: {self.y}, Name: {self.name}"

    @staticmethod
    def get_random(min, max, name):
        x = random.uniform(min, max)
        y = random.uniform(min, max)
        return City(x, y, name)


class Ant:
    def __init__(self, cities, start_city_index, pheromones, alpha, beta, inverse_distances, distances):
        self.__cities = cities
        self.__start_city_index = start_city_index
        self.__pheromones = pheromones
        self.total_distance = 0
        self.path = [start_city_index]
        self.__alpha = alpha
        self.__beta = beta
        self.__actual_city_idx = start_city_index
        self.__inverse_distances = inverse_distances
        self.__distances = distances

    def reset_ant(self):
        self.total_distance = 0
        self.path = [self.__start_city_index]
        self.__actual_city_idx = self.__start_city_index

    def select_next(self):
        if len(self.__cities) == len(self.path):
            return

        propabilities = {}

        for i, city in enumerate(self.__cities):
            try:
                self.path.index(i)
            except ValueError:
                propabilities[i] = self.__pheromones[self.__actual_city_idx][i] ** alpha \
                                   + self.__inverse_distances[self.__actual_city_idx][i] ** beta

        propabilities = {k: v for k, v in sorted(propabilities.items(), key=lambda item: item[1])}
        for key in propabilities.keys():
            propabilities[key] /= sum(propabilities.values())
        random_numer = random.uniform(0, 1)
        last_key = 0
        for idx, key in enumerate(propabilities.keys()):
            if idx == 0:
                if random_numer < propabilities[key]:
                    self.path.append(key)
                    self.__actual_city_idx = key
            elif last_key != key and propabilities[last_key] <= random_numer <= propabilities[key]:
                self.path.append(key)
                self.__actual_city_idx = key
            elif idx == len(propabilities.keys()) - 1:
                self.path.append(key)
                self.__actual_city_idx = key
            last_key = key

    def calculate_distance(self):
        for i in range(0, (len(self.path) - 1)):
            self.total_distance += self.__distances[self.path[i - 1]][self.path[i]]

    def __len__(self):
        return len(self.__cities)


class Solution:
    def __init__(self, count_of_cities, alpha, beta, vaporization_coefficient, Q):
        self.__cities = self.__generate_cities(count_of_cities)
        self.__distances, self.__inverse_distances = self.__get_distances()
        self.__pheromone_matrix = [[1 for x in range(count_of_cities)] for y in range(count_of_cities)]
        self.__alpha = alpha
        self.__beta = beta
        self.__vaporization_coefficient = vaporization_coefficient
        self.__Q = Q

    def __generate_cities(self, count_of_cities):
        cities = []
        for i in range(count_of_cities):
            name = chr(i + 97)
            cities.append(City.get_random(minimum, max, name))
        return cities

    def __get_distances(self):
        distance_matrix = [[0 for _ in range(len(self.__cities))] for _ in range(len(self.__cities))]
        inverse_matrix = [[0 for _ in range(len(self.__cities))] for _ in range(len(self.__cities))]

        for i in range(count_of_cities):
            for j in range(count_of_cities):
                if i == j:
                    continue
                if distance_matrix[i][j] == 0:
                    distance = np.sqrt((self.__cities[i].x - self.__cities[j].x) ** 2 + (self.__cities[i].y - self.__cities[j].y) ** 2)
                    distance_matrix[i][j] = distance
                    distance_matrix[j][i] = distance
                    inverse_matrix[i][j] = 1 / distance
                    inverse_matrix[j][i] = 1 / distance
        return distance_matrix, inverse_matrix

    def __get_ants(self,):
        ants = []
        for i in range(len(self.__cities)):
            ants.append(Ant(self.__cities, i, self.__pheromone_matrix, self.__alpha, self.__beta, self.__inverse_distances, self.__distances))
        return ants

    def travel_salesman(self, G):
        population = self.__get_ants()
        best_ants = []
        best_distances = []
        for i in range(G):
            for ant in population:
                ant.reset_ant()
                for i in range(len(self.__cities)):
                    ant.select_next()
                ant.calculate_distance()
            best = self.__get_best_individual(population)
            best_ants.append(copy.deepcopy(best))
            best_distances.append(best.total_distance)
            self.__recalculate_pheromones(population)

        self.__visualize_paths(best_ants)
        self.__draw_graph_with_minimal_values_found(best_distances)

        absolute_best = best_ants[0]
        absolute_best_distance = best_distances[0]
        for i in range(G):
            if best_distances[i] < absolute_best_distance:
                absolute_best_distance = best_distances[i]
                absolute_best = best_ants[i]
        self.__visualize_best(absolute_best)
        return absolute_best

    def __visualize_cities(self):
        fig, ax = plt.subplots()
        cities = self.__cities
        ys = [cities[i].y for i in range(len(cities))]
        xs = [cities[i].x for i in range(len(cities))]
        names = [cities[i].name for i in range(len(cities))]

        plt.scatter(xs, ys)
        for i in range(len(names)):
            plt.annotate(names[i], (xs[i], ys[i]), textcoords="offset points", xytext=(0, 10))
        return fig, plt

    def __visualize_best(self, ant):
        fig, plt = self.__visualize_cities()
        ys = [self.__cities[ant.path[i]].y for i in range(len(self.__cities))]
        xs = [self.__cities[ant.path[i]].x for i in range(len(self.__cities))]
        xs.append(xs[0])
        ys.append(ys[0])
        plt.plot(xs, ys)
        plt.show()

    def __recalculate_pheromones(self, ants):
        for i in range(len(self.__cities)):
            for j in range(len(self.__cities)):
                self.__pheromone_matrix[i][j] = self.__pheromone_matrix[i][j] \
                                                - (self.__vaporization_coefficient * self.__pheromone_matrix[i][j])

        for ant in ants:
            reverse_dist = self.__Q / ant.total_distance
            path = ant.path
            for i in range(len(self.__cities)):
                from_idx = path[i-1]
                to_idx = path[i]
                self.__pheromone_matrix[from_idx][to_idx] += reverse_dist

    def __visualize_paths(self, individuals):
        fig, plt = self.__visualize_cities()

        xxs = []
        yys = []

        for individual in individuals:
            path = individual.path
            ys = [self.__cities[path[i]].y for i in range(len(self.__cities))]
            xs = [self.__cities[path[i]].x for i in range(len(self.__cities))]
            xs.append(xs[0])
            ys.append(ys[0])
            xxs.append(xs)
            yys.append(ys)

        lines = plt.plot(xxs[0], yys[0])

        def update(iteration, lines):
            lines.set_xdata(xxs[iteration])
            lines.set_ydata(yys[iteration])
        anim = FuncAnimation(fig, update, len(individuals), fargs=(lines), interval=200, repeat=False)
        plt.show()

    def __draw_graph_with_minimal_values_found(self, real_min_values):
        x = np.arange(len(real_min_values))
        plt.plot(x, real_min_values, color='c', linewidth=1.5, label='Function evaluation')
        plt.xlabel("Number of generations")
        plt.ylabel("Function evaluation")
        plt.legend()
        plt.show()

    def __get_best_individual(self, population):
        minimum = np.inf
        min_idx = None
        for idx, individual in enumerate(population):
            if individual.total_distance < minimum:
                min_idx = idx
                minimum = individual.total_distance
        return population[min_idx]


minimum = 0
max = 100

count_of_cities = 20
alpha = 1
beta = 2
G = 200
vaporization_coefficient = 0.5
Q = 1

s = Solution(count_of_cities, alpha, beta, vaporization_coefficient, Q)
s.travel_salesman(G)
