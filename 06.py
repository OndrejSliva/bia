import matplotlib.pyplot as plt
import numpy as np
import random
from matplotlib.animation import FuncAnimation
import mpl_toolkits.mplot3d.axes3d as p3
import copy


class ControlParameters:
    def __init__(self, pop_size, M_max, c_1, c_2, v_min, v_max, function, up_bound, low_bound):
        self.population_size = pop_size
        self.M_max = M_max
        self.c_1 = c_1
        self.c_2 = c_2
        self.v_min = v_min
        self.v_max = v_max
        self.function = function
        self.up_bound = up_bound
        self.low_bound = low_bound


class Individual:
    def __init__(self, parameters, function, i):
        self.__parameters = parameters
        self.__best_parameters = copy.deepcopy(parameters)
        self.__parameter_history = [copy.deepcopy(self.__parameters)]
        self.__evaluation = np.inf
        self.__best_evaluation = np.inf
        self.__evaluate(function)
        self.__evaluation_history = [self.__evaluation]
        self.__velocity = np.array([0.0] * len(self.__parameters))
        self.__i = i

    def __evaluate(self, function):
        self.__evaluation = function(self.__parameters)

    def count_velocity(self, control_parameters, best_parameters):
        r_1 = random.uniform(0, 1)
        c_1 = control_parameters.c_1
        c_2 = control_parameters.c_2
        self.__velocity = self.__velocity * self.__w(control_parameters.M_max) \
                          + r_1 * c_1 * (self.__best_parameters - self.__parameters) \
                          + r_1 * c_2 * (best_parameters - self.__parameters)
        for i, value in enumerate(self.__velocity):
            if value > control_parameters.v_max:
                self.__velocity[i] = control_parameters.v_max
            elif value < control_parameters.v_min:
                self.__velocity[i] = control_parameters.v_min

    def get_coordinates(self, index, is_3D = True):
        x = self.__parameter_history[index][0]
        y = self.__parameter_history[index][1]
        if is_3D:
            z = self.__evaluation_history[index]
            return x, y, z
        else:
            return x, y

    def __w(self, M_max):
        w_s = 0.9
        w_e = 0.4
        return w_s * ((w_s - w_e) * self.__i) / M_max

    def get_parameters(self):
        return copy.deepcopy(self.__parameters)

    def get_evaluation(self):
        return self.__evaluation

    def update_parameters(self, control_parameters, function):
        self.__parameters += self.__velocity
        for i, value in enumerate(self.__parameters):
            if value > control_parameters.up_bound:
                self.__parameters[i] = control_parameters.up_bound
            elif value < control_parameters.low_bound:
                self.__parameters[i] = control_parameters.low_bound
        self.__evaluate(function)
        self.__add_to_history()

    def __add_to_history(self):
        self.__parameter_history.append(copy.deepcopy(self.__parameters))
        self.__evaluation_history.append(copy.deepcopy(self.__evaluation))

    def compare_best(self):
        if self.__evaluation < self.__best_evaluation:
            self.__best_parameters = self.__parameters
            self.__best_evaluation = self.__evaluation

    def __len__(self):
        return len(self.__parameters)

    @staticmethod
    def create_random(dimension, lower_bound, upper_bound, function, i):
        parameters = []
        for d in range(dimension):
            parameters.append(random.uniform(lower_bound, upper_bound))
        return Individual(np.array(parameters), function, i)


class Population:
    def __init__(self, individuals, control_parameters):
        self.individuals = individuals
        self.__best_individual = self.get_best_individual()
        self.__control_parameters = control_parameters

    def get_best_evaluation(self):
        best_evaluation = np.inf
        for individual in self.individuals:
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
        return best_evaluation

    def get_best_individual(self):
        best_evaluation = np.inf
        best_individual = None
        for individual in self.individuals:
            if individual.get_evaluation() < best_evaluation:
                best_evaluation = individual.get_evaluation()
                best_individual = individual
        return best_individual

    def update_velocity(self):
        for individual in self.individuals:
            individual.count_velocity(self.__control_parameters, self.__best_individual.get_parameters())

    def get_individuals(self):
        return self.individuals

    def update(self, function):
        for individual in self.individuals:
            #Calculate a new velocity v for a particle x  # Check boundaries of velocity (v_mini, v_maxi)
            individual.count_velocity(self.__control_parameters, self.__best_individual.get_parameters())
            #Calculate a new position for a particle x  # Old position is always replaced by a new position. CHECK BOUNDARIES!
            individual.update_parameters(self.__control_parameters, function)
            #Compare a new position of a particle x to its pBest
            individual.compare_best()

            if individual.get_evaluation() < self.__best_individual.get_evaluation():
                self.__best_individual = copy.deepcopy(individual)

    def get_count_of_iterations(self):
        return self.__control_parameters.M_max

    def __len__(self):
        return len(self.individuals)


class Solution:
    def __init__(self, dimension, lower_bound, upper_bound):
        self.dimension = dimension
        self.lower_bound = lower_bound  # we will use the same bounds for all parameters
        self.upper_bound = upper_bound

    def __generate_random_population(self, function, control_parameters):
        population = []
        for i in range(control_parameters.population_size):
            population.append(Individual.create_random(self.dimension, self.lower_bound, self.upper_bound, function, i))
        return Population(population, control_parameters)

    def particle_swarm_optimalization(self, function, control_parameters):
        population = self.__generate_random_population(function, control_parameters)
        population.update_velocity()    #For each particle, generate velocity vector v

        min_values = []

        for m in range(control_parameters.M_max):
            population.update(function)
            min_values.append(copy.deepcopy(population.get_best_evaluation()))

        if self.dimension == 2:
            X, Y, Z = self.get_data_for_draw(function, 0.25)
            self.draw(X, Y, Z, population)
            self.draw_heat_map(Z, population)
        self.draw_graph_with_minimal_values_found(min_values)

    def draw_graph_with_minimal_values_found(self, real_min_values):
        x = np.arange(len(real_min_values))
        plt.plot(x, real_min_values, color='c', linewidth=1.5, label='Function evaluation')
        plt.xlabel("Number of generations")
        plt.ylabel("Function evaluation")
        plt.legend()
        plt.show()

    def get_data_for_draw(self, function, step):
        x = np.arange(self.lower_bound, self.upper_bound + step, step)
        y = np.arange(self.lower_bound, self.upper_bound + step, step)
        X, Y = np.meshgrid(x, y)
        Z = function([X, Y])
        return X, Y, Z

    def draw(self, X, Y, Z, populations):
        fig = plt.figure()
        ax = p3.Axes3D(fig)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('f(x1, x2)')
        ax.plot_surface(X, Y, Z, alpha=0.2)

        xxs = []
        yys = []
        zzs = []

        for i in range(populations.get_count_of_iterations()):
            xs = []
            ys = []
            zs = []
            for individual in populations.get_individuals():
                x, y, z = individual.get_coordinates(i)
                xs.append(x)
                ys.append(y)
                zs.append(z)
            xxs.append(xs)
            yys.append(ys)
            zzs.append(zs)

        def update(iteration, points):
            points.set_data_3d(xxs[iteration], yys[iteration], zzs[iteration])
        points, = ax.plot(xxs[0], yys[0], zzs[0], 'o')
        anim = FuncAnimation(fig, update, populations.get_count_of_iterations(), fargs=[points], interval=200)

        plt.show()

    def draw_heat_map(self, Z, populations):
        fig, ax = plt.subplots()
        ax.imshow(Z)

        plt.xlim(self.lower_bound, self.upper_bound)
        plt.ylim(self.lower_bound, self.upper_bound)
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')

        plt.imshow(Z, cmap='Spectral_r', extent=[self.lower_bound, self.upper_bound, self.lower_bound, self.upper_bound])

        xxs = []
        yys = []

        for i in range(populations.get_count_of_iterations()):
            xs = []
            ys = []
            for individual in populations.get_individuals():
                x, y = individual.get_coordinates(i, False)
                xs.append(x)
                ys.append(y)
            xxs.append(xs)
            yys.append(ys)

        def update(iteration, point):
            point.set_data(xxs[iteration], yys[iteration])
            return point

        point, = ax.plot(xxs[0], yys[0], 'o', color='k')
        anim = FuncAnimation(fig, update, populations.get_count_of_iterations(), fargs=[point])
        plt.show()


class Function:
    def sphere(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2
        return sum

    def schwefel(self, params):
        sum = 0.0
        for param in params:
            sum += param * np.sin(np.sqrt(np.abs(param)))
        return (418.9829 * len(params)) - sum

    def rosenbrock(self, params):
        sum = 0.0
        for i in range(len(params) - 1):
            sum += 100 * (params[i + 1] - params[i] ** 2) ** 2 + (params[i] - 1) ** 2
        return sum

    def rastrigin(self, params):
        sum = 0.0
        for param in params:
            sum += param ** 2 - 10 * np.cos(2 * np.pi * param)
        return 10 * len(params) + sum

    def griewank(self, params):
        sum = 0.0
        mult = 1.0
        for i, x in enumerate(params):
            sum += x ** 2 / 4000
            mult *= np.cos(x / np.sqrt(i + 1))
        return sum - mult + 1

    def levy(self, params):
        def w(x):
            return 1 + (x - 1) / 4
        sum = 0.0
        for i in range(len(params) - 1):
            sum += (w(params[i]) - 1) ** 2 * (1 + 10 * np.sin(np.pi * w(params[i]) + 1) ** 2) + (w(params[len(params) - 1]) - 1) ** 2 * (1 + np.sin(2 * np.pi * w(params[len(params) - 1])) ** 2)
        return np.sin(np.pi * w(params[0])) ** 2 + sum

    def michalewicz(self, params):
        m = 10
        sum = 0.0
        for i, param in enumerate(params):
            sum += np.sin(param) * np.sin(((i + 1) * param ** 2) / np.pi) ** (2 * m)
        return -sum

    def zakharov(self, params):
        sum1 = 0.0
        sum2 = 0.0
        for i, param in enumerate(params):
            sum1 += param ** 2
            sum2 += 0.5 * (i + 1) * param
        return sum1 + sum2 ** 2 + sum2 ** 4

    def ackley(self, params):
        a = 20.0
        b = 0.2
        c = 2 * np.pi
        sum1 = 0.0
        sum2 = 0.0
        for param in params:
            sum1 += param ** 2
            sum2 += np.cos(c * param)
        return -a * np.exp(-b * np.sqrt(sum1 / len(params))) - np.exp(sum2 / len(params)) + a + np.exp(1)


def run(function, low_bound, up_bound, pop_size, M_max, c_1, c_2, v_min=None, v_max=None):
    if v_min is None or v_max is None:
        v_min = low_bound / 10
        v_max = up_bound / 10
    control_parameters = ControlParameters(pop_size, M_max, c_1, c_2, v_min, v_max, Function().sphere, up_bound, low_bound)
    s = Solution(2, low_bound, up_bound)
    s.particle_swarm_optimalization(function, control_parameters)


pop_size = 20
M_max = 200
c_1 = 1
c_2 = 1

run(Function().sphere, -5, 5, pop_size, M_max, c_1, c_2)
run(Function().schwefel, -500, 500, pop_size, M_max, c_1, c_2)
run(Function().rosenbrock, -10, 10, pop_size, M_max, c_1, c_2)
run(Function().rastrigin, -5.12, 5.12, pop_size, M_max, c_1, c_2)
run(Function().griewank, -10, 10, pop_size, M_max, c_1, c_2)
run(Function().levy, -10, 10, pop_size, M_max, c_1, c_2)
run(Function().michalewicz, 0, np.pi, pop_size, M_max, c_1, c_2)
run(Function().zakharov, -10, 10, pop_size, M_max, c_1, c_2)
run(Function().ackley, -32.768, 32.768, pop_size, M_max, c_1, c_2)

